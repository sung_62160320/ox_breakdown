/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumsung.ox_breakwork;

import java.util.Scanner;

public class OX_Breakfun {
    static int turn = 1;
    static char winner = '-';
    static boolean isFinish = false;
    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static char[][] table
            = {{'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");

    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Plase input Row Col:");
            row = kb.nextInt()-1;
            col = kb.nextInt()-1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            } else {
                System.out.println("Error: table at row and col "
                        + "is not empty!!!");
                continue;
                
            }
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX_Left() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (row == col && table[row][col] != player) {
                    return;
                }
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX_Right() {
        int col = 2;
        int row = 0;
        for (int round = 0; round < 3; round++) {
            if (table[row][col] != player) {
                return;
            }
            col--;
            row++;
        }
        isFinish = true;
        winner = player;
    }
    
    static void checkDraw(){
        if(turn == 9 && isFinish == false && winner == '-'){
            isFinish = true;
        }
    }
    
    static void checkWin() {
        checkCol();
        checkRow();
        checkX_Left();
        checkX_Right();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
        turn++;
    }

    static void showResult() {
        if (winner == '-' ) {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showTable();
        showResult();
        showBye();
    }
}
